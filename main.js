
const KEY_JUGADORES = 'JUGADORES';
let listadoReservas = [];

const agregarJugadorTabla = (data) => {
  $(".Nombre").append(`<p style='color:#FFD306'>${data.nombre} ${data.apellido} / ${data.nombre2} ${data.apellido2}</p>`)
  $(".Categoria").append(`<p style='color:#FFD306'>${data.categoria}</p>`)
  $(".Torneo").append(`<p style='color:#FFD306'>${data.torneo}</p>`)
}

const listadoLocalStorage = localStorage.getItem(KEY_JUGADORES);
if (listadoLocalStorage) {
  listadoReservas = JSON.parse(listadoLocalStorage);
}

//OBTENER EL FORMULARIO SOBRE EL QUE VAMOS A ESCUCHAR EL EVENTO SUBMIT
//DEFINIMOS EL MANEJADOR DE EVENTOS USANDO LA PROPIEDAD "onsubmit"
document.getElementById("miFormulario").onsubmit = (event) => {
    //EMPLEAMOS EL PARAMETRO EVENT PARA PREVENIR EL COMPORTAMIENTO POR DEFECTO DEL FORMULARIO
    event.preventDefault();
    const data = new FormData(event.target);

    //CREO UN OBJETO EMPLEANDO LOS VALUE DEL FORMULARIO.
    const dataFormulario = {
        nombre: data.get("nombre"),
        nombre2: data.get("nombre2"),
        apellido: data.get("apellido"),
        apellido2: data.get("apellido2"),
        categoria: data.get("categoria"),
        torneo: data.get("torneo")
    }

    agregarJugadorTabla(dataFormulario);
    listadoReservas.push(dataFormulario);
    localStorage.setItem(KEY_JUGADORES, JSON.stringify(listadoReservas));
    document.getElementById("miFormulario");
}



$('#categoria').change((e) => {
    const inputs = $('#categoria');
    const asociado = $(inputs).val();

    if (asociado === "C8") {
        e.preventDefault();
        $(".p-torneoc8").fadeIn()
        $(".inputsc8").fadeIn()
        $(".inputsc8").empty();
        $(".inputsc8").prepend(`<option  value="DOMINGO 22/05 A LAS 15:00HS">DOMINGO 22/05 A LAS 15:00HS</option>`);
        
    } else if (asociado === "C7") {
        e.preventDefault();
        $(".inputsc8").empty();
        $(".inputsc8").prepend(`<option  value="SABADO 22/05 A LAS 10:00HS">SABADO 22/05 A LAS 10:00HS</option>`);
        $(".p-torneoc8").fadeIn()
        $(".inputsc8").fadeIn()
    } else if (asociado === "C6") {
        e.preventDefault();
        $(".inputsc8").empty();
        $(".inputsc8").prepend(`<option  value="VIERNES 22/05 A LAS 10:00HS">VIERNES 22/05 A LAS 10:00HS</option>`);
        $(".p-torneoc8").fadeIn()
        $(".inputsc8").fadeIn()
    } else if (asociado === "C5") {
        e.preventDefault();
        $(".inputsc8").empty();
        $(".inputsc8").prepend(`<option  value="DOMINGO 22/05 A LAS 13:00HS">DOMINGO 22/05 A LAS 13:00HS</option>`);
        $(".p-torneoc8").fadeIn()
        $(".inputsc8").fadeIn()
    }

    $(".tabla-container p").remove();

    $(".div-tabla").fadeIn(1000, function () {
      $(".titulo-tabla").fadeIn()
      $(".tabla-container").fadeOut();
      $(".tabla-container").fadeIn(500);
    });

    const listadoReservasPorCategoria = listadoReservas.filter((reserva) => {
      return reserva.categoria === asociado;
    });

    listadoReservasPorCategoria.forEach((reserva) => {
      agregarJugadorTabla(reserva)
    })
});


















